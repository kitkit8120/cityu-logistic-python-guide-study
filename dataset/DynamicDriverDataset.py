import datetime
import os, sys, inspect

from dataset.NodeDataset import nodeDataset
from modelObjects.VehicleLocation import VehicleLocation
from modelObjects.Order import Order
from utils.util import getNearestNodeByLatLng

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0, parentdir)

from Common import Common
import requests
import json
from utils.SingletonMetaclass import SingletonMetaclass
from modelObjects.DynamicDriver import  DynamicDriver
from modelObjects.Delivery import Delivery
from dataset.OrderDataset import orderDataset
from dataset.DriverVehicleDataset import driverVehicleDataset
from dataset.DepotDataset import depotDataset

class DynamicDriverDataset(metaclass=SingletonMetaclass):
    def __init__(self):

        self.dynamicDriverList = []

    def loadingDriverWithDelivery(self,date):

        path = Common.apiPath + "deliveries/getAPIDynamicDrivers"
        data = {"date": date}
        # print("loadingDriverWithDelivery date",date)
        r = requests.post(path, data=data)

        jsonstr = json.loads(r.text)

        for row in jsonstr:
            driverVehicleJson = row["driverVehicle"]
            dynamicDriver = DynamicDriver()
            driverVehicle = driverVehicleDataset.getDriverVehicleByVIdandDId(driverVehicleJson['vehicleId'],driverVehicleJson['driverId'])
            dynamicDriver.driverVehicle = driverVehicle
            vehicleLocation = VehicleLocation()
            vehicleLocation.parseData(row['vehicleLocation'])
            vehicleLocation.node = getNearestNodeByLatLng(vehicleLocation.lat,vehicleLocation.lng)
            dynamicDriver.vehicleLocation= vehicleLocation
            driverLocationDeliveryObject = self.getDriverLocationDelivery(vehicleLocation.node,driverVehicle)

            dynamicDriver.remainDeliveries.append(driverLocationDeliveryObject)

            for delivery in row['remainDeliveries']:
                if str(delivery['orderId']) == "0" or str(delivery['orderId']) == "-1" or str(delivery['orderId']) == "Depot":
                    continue

                deliveryObject = Delivery()
                deliveryObject.driverVehicle = dynamicDriver.driverVehicle
                deliveryObject.deliveryDate = datetime.datetime.strptime(delivery["deliveryCalDate"], '%Y-%m-%d')
                deliveryObject.deliveryTime = datetime.datetime.strptime(delivery["deliveryTime"], '%Y-%m-%d %H:%M:%S')
                deliveryObject.aStarNodeList = None
                deliveryObject.serviceTime = float(delivery["serviceTime"])
                deliveryObject.order = orderDataset.getOrderByOrderId(delivery['orderId'])
                dynamicDriver.remainDeliveries.append(deliveryObject)

            returnDepotDelivery = self.getDriverLocationDelivery(
                depotDataset.depot.getNearestNode(), dynamicDriver)
            returnDepotDelivery.serviceTime = 600
            returnDepotDelivery.order.orderId =0
            dynamicDriver.remainDeliveries.append(returnDepotDelivery)
            self.dynamicDriverList.append(dynamicDriver)

        # print(self.dynamicDriverList)
        # print(len(self.dynamicDriverList))
        # exit()

    def getDriverLocationDelivery(self, driverLocationNode,driverVehicle,):
        order = Order()
        order.orderId = -1
        order.setNearestNode(driverLocationNode)
        order.deliveryTimeStart = Common.startTimeWindow
        order.deliveryTimeEnd = Common.endTimeWindow

        deliveryObject = Delivery()
        deliveryObject.driverVehicle = driverVehicle
        deliveryObject.order = order

        deliveryObject.serviceTime = float(0)
        return deliveryObject

    # def loadingStaticDriver(self):
    #     driverVehicleDataset.loadingData()
    #     for d1 in driverVehicleDataset.driverVehicles:
    #         blInside = False
    #         for d2 in self.dynamicDriverList:
    #             if d1.driverId == d2.driverVehicle.driverId:
    #                 blInside = True
    #         if blInside ==False:
    #             dynamicDriver = DyanmicDriverObject()
    #             dynamicDriver.driverVehicle = d1
    #             dynamicDriver.driverLocationNodeId = depotDataset.depot.getNearestNode().nodeId
    #             deliveryObject = self.getDriverLocationDelivery(
    #                 depotDataset.depot.getNearestNode(), d1)
    #             dynamicDriver.remainDeliveries.append(deliveryObject)
    #             returnDepotDelivery = self.getDriverLocationDelivery(
    #                 depotDataset.depot.getNearestNode(), d1)
    #
    #             returnDepotDelivery.serviceTime = 600
    #             dynamicDriver.remainDeliveries.append(deliveryObject)
    #             dynamicDriver.remainDeliveries.append(returnDepotDelivery)
    #             self.dynamicDriverList.append(dynamicDriver)


    def loadingData(self, date):
        self.loadingDriverWithDelivery(date)
        # self.loadingStaticDriver()
        print("Dynamic Driver Dataset initialized")
        # print(self.dynamicDriverList)


dynamicDriverDataset = DynamicDriverDataset()
