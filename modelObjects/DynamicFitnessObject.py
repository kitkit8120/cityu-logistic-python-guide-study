from utils.util import diffBetweenDateTime


class DynamicFitnessObject:

    def __init__(self, singleDynamicDriver, dynamicDriver):
        self.singleDynamicDriver = singleDynamicDriver
        self.dynamicDriver = dynamicDriver
        # self.mutliDeliveriesList = list(o.remainDeliveries for o in dynamicDriverList)
        self.totalServiceTime = 0
        self.totalNewDuration = 0
        self.fitnessValue = 0
        self.originTotalDuration = 0

    def orignalRouteFitness(self):
        startDelivery = self.dynamicDriver.remainDeliveries[0]
        endDelivery = self.dynamicDriver.remainDeliveries[-1]
        endDeliveryTime = endDelivery.deliveryTime
        originTotalDuration = diffBetweenDateTime(endDeliveryTime, startDelivery.deliveryTime)
        self.originTotalDuration = originTotalDuration
    def routeFitness(self):
        self.orignalRouteFitness()
        totalDuration = 0
        serviceTime = 0


        for delivery in self.singleDynamicDriver.remainDeliveries:
            serviceTime += delivery.serviceTime
            delivery.driverVehicle = self.singleDynamicDriver.driverVehicle

        startDelivery = self.singleDynamicDriver.remainDeliveries[0]
        endDelivery = self.singleDynamicDriver.remainDeliveries[-1]
        endDeliveryTime = endDelivery.deliveryTime
        totalNewDuration = diffBetweenDateTime(endDeliveryTime,startDelivery.deliveryTime)
        self.totalServiceTime = serviceTime
        self.totalNewDuration = totalNewDuration

        self.fitnessValue = 3600 / (self.totalNewDuration-self.originTotalDuration)  # 1 hour: 3600 seconds
        return self.fitnessValue

    def __repr__(self):
        return "diff duration: %s, total New Duration : %s, total old Duration : %s, driverList :%s" %((self.totalNewDuration-self.originTotalDuration), self.totalNewDuration,self.originTotalDuration, self.singleDynamicDriver)
