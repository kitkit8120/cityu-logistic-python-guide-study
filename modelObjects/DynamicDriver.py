


class DynamicDriver:
    def __init__(self,):
        self.driverVehicle=None
        self.remainDeliveries = []
        self.vehicleLocation = None

    def __repr__(self):
        str = "driverVehicle %s \n"%(self.driverVehicle)
        str += "vehicleLocation %s \n" % (self.vehicleLocation)
        # str = ""
        for index in range(len(self.remainDeliveries)-1):
            startDelivery = self.remainDeliveries[index]
            endDelivery = self.remainDeliveries[index+1]
            str +='[Done from %s to %s] \n' % (
            startDelivery.order, endDelivery.order)
        return str


