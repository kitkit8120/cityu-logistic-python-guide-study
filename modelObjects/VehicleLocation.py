
# coding: utf-8

# In[1]:
from modelObjects.Order import Order
import datetime

from utils.util import stringToDate


class VehicleLocation:
    def __init__(self):
        self.vehicleId = None
        self.lat = None
        self.lng = None
        self.node = None
        self.createDate = None

    def parseData(self, row):
        self.vehicleId = row['vehicleId']
        self.lat = float(row['latitude'])
        self.lng = float(row['longitude'])
        self.createDate = stringToDate(row['createDate'])

    def __repr__(self):
        return "vehicleId ID {}, Node {}: ({},{})".format(self.vehicleId,self.node, self.lat, self.lng)