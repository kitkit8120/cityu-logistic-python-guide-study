import datetime

from utils.SingletonMetaclass import SingletonMetaclass


class Common(metaclass=SingletonMetaclass):
    
    def __init__(self):
        self.apiPath = 'http://localhost:8888/logistic/index.php/'
        # self.apiPath = "https://deep-rec.com/logistic/index.php/"
        self.deliveryDate = None
        self.startTimeWindow = None
        self.endTimeWindow = None
        self.thread = 1
        self.len_population = 10
        self.crossRate = 0.2
        self.mutationRate = 0.04
        self.max_generations = 2
        self.len_max_keep = 2

Common = Common()

