

import datetime


from DataInitializer import dataInitializer
import multiprocessing
import argparse

from services.ResultService import resultService

parser = argparse.ArgumentParser()
parser.add_argument("-s","--startTimeWindow", type=str, default="2019-11-30 10:00:00", help="The start time of delivery date")
parser.add_argument("-e","--endTimeWindow", type=str, default="2019-11-30 22:00:00", help="The end time of delivery date")
args = parser.parse_args()

if __name__ == '__main__':
    multiprocessing.freeze_support()
    startDate = datetime.datetime.strptime(args.startTimeWindow, '%Y-%m-%d %H:%M:%S')
    endDate = datetime.datetime.strptime(args.endTimeWindow, '%Y-%m-%d %H:%M:%S')
    dataInitializer.setStartDate(startDate)
    dataInitializer.setTimeEndWindows(endDate)
    dataInitializer.normalGenerate(startDate.strftime("%Y-%m-%d"))

    from services.TravellingService import travellingService
    travellingService.method = 0

    from services.NearestNeighborService import nearestNeighborService
    fitnessResult1 = nearestNeighborService.run()

    from services.GeneticAlgorithmService import geneticAlgorithmService
    fitnessResult = geneticAlgorithmService.run()
    # print(fitnessResult.multiDeliveriesList)

    resultService.sendResultToDB(fitnessResult)
    print("Done - please check the route plan in the website.")

