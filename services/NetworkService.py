import multiprocessing
import os, sys, inspect
import time
from multiprocessing.pool import ThreadPool

from Common import Common
from dataset.DepotDataset import depotDataset
from dataset.OrderDataset import orderDataset

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0, parentdir)
from dataset.EdgeDataset import edgeDataset
from dataset.NodeDataset import nodeDataset
from dataset.DurationDataset import durationDataset
import networkx as nx
import random
from utils.SingletonMetaclass import SingletonMetaclass
import datetime
import numpy as np


class NetworkService(metaclass=SingletonMetaclass):
    def __init__(self, ):

        self.initGraph()
        self.progress = 0;
        # self.buildDynamicNetorkXNpz()

    def initGraph(self, ):
        self.G = nx.DiGraph()
        nodeIds = list(nodeDataset.nodes.keys())
        self.G.add_nodes_from(nodeIds)

        edges = []
        for startPoint in edgeDataset.nodesLink:
            for endPoint in edgeDataset.nodesLink[startPoint]:
                edge = edgeDataset.nodesLink[startPoint][endPoint]
                value = durationDataset.getStaticDurationByEdgeId(edge.v_id)
                edges.append((startPoint, endPoint, value))

        self.G.add_weighted_edges_from(edges)

    def buildDynamicNetorkXNpz(self,date):
        startDateTime = Common.startTimeWindow
        endDateTime = Common.endTimeWindow
        current = startDateTime
        count = 0
        self.timeArray = []
        while current < endDateTime:
            count += 1
            self.timeArray.append(current)
            newTime = current + datetime.timedelta(minutes=2)
            current = newTime

        orders = orderDataset.orders
        depot = depotDataset.depot
        orderNodes = (o.nearestNode.nodeId for o in orders)
        self.nodeSet = [depot.nearestNode.nodeId] + list(orderNodes)

        self.durationNp = np.zeros((len(self.timeArray), len(self.nodeSet), len(self.nodeSet)))

        print(self.durationNp.shape)
        # for index in range(len(self.timeArray)):
        #     self.multiGraphicInit(index)

        # with ThreadPool(processes=10) as p:
        #     duration = p.map(self.multiProcessingGraphicInit, range(len(self.timeArray)))

        with multiprocessing.Pool(36) as p:
            totalDurationNp = p.map(self.multiProcessingGraphicInit, self.timeArray)
        # for durationTimelotIndex in range(len(totalDurationNp)):
        #     durationTimelot = totalDurationNp[durationTimelotIndex];
        #     self.durationNp(durationTimelotIndex,)
        self.durationNp = np.asarray(totalDurationNp)
        # print(self.durationNp)
        # print(self.durationNp.shape)

        np.savez("data/%s"%(date), nodeId=np.asarray(self.nodeSet), time=np.asarray(self.timeArray),
                 duration=self.durationNp)

    #
# def multiGraphicInit(self, timelotIndex):
    #     self.progress+=1
    #     print("%s/%s" %(self.progress,len(self.timeArray)))
    #     G = nx.create_empty_copy(self.G)
    #     edges = []
    #     for startPoint in edgeDataset.nodesLink:
    #         for endPoint in edgeDataset.nodesLink[startPoint]:
    #             edge = edgeDataset.nodesLink[startPoint][endPoint]
    #             value = durationDataset.getDynamicDurationByEdgeIdAndTime(edge.v_id, self.timeArray[timelotIndex])
    #             edges.append((startPoint, endPoint, value))
    #     G.add_weighted_edges_from(edges)
    #
    #     for startIndex in range(len(self.nodeSet)):
    #         for endIndex in range(len(self.nodeSet)):
    #             if startIndex == endIndex:
    #                 self.durationNp[timelotIndex, startIndex, endIndex] = 0
    #             else:
    #                 startNodeId = self.nodeSet[startIndex]
    #                 endNodeId = self.nodeSet[endIndex]
    #                 self.durationNp[timelotIndex, startIndex, endIndex] = float(
    #                     nx.astar_path_length(G, startNodeId, endNodeId))

    def multiProcessingGraphicInit(self, timelot):
        # self.progress+=1
        # print("%s/%s" %(self.progress,len(self.timeArray)))
        print(timelot)
        G = nx.create_empty_copy(self.G)
        edges = []
        durationNp =np.zeros((len(self.nodeSet), len(self.nodeSet)))
        for startPoint in edgeDataset.nodesLink:
            for endPoint in edgeDataset.nodesLink[startPoint]:
                edge = edgeDataset.nodesLink[startPoint][endPoint]
                value = durationDataset.getDynamicDurationByEdgeIdAndTime(edge.v_id, timelot)
                edges.append((startPoint, endPoint, value))
        G.add_weighted_edges_from(edges)

        for startIndex in range(len(self.nodeSet)):
            for endIndex in range(len(self.nodeSet)):
                if startIndex == endIndex:
                    durationNp[startIndex,endIndex] = 0
                else:
                    startNodeId = self.nodeSet[startIndex]
                    endNodeId = self.nodeSet[endIndex]
                    durationNp[startIndex,endIndex] = float(
                        nx.astar_path_length(G, startNodeId, endNodeId))
        return durationNp


networkService = NetworkService()
