import datetime
import os,sys,inspect

from dataset.EdgeDataset import edgeDataset
from services.AStarService import aStarService

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir) 

from dataset.DriverVehicleDataset import driverVehicleDataset

from Common import Common
import requests
import json
from utils.SingletonMetaclass import SingletonMetaclass
from utils.util import myconverter
from utils.util import diffBetweenDateTime


class ResultService(metaclass=SingletonMetaclass):
    def __init__(self):
        pass

    def doAStarSearch(self,startDelivery,endDelivery):
        nodeList, _ = aStarService.doSearch(startDelivery.order.nearestNode, endDelivery.order.nearestNode,
                                  startDelivery.deliveryTime + datetime.timedelta(
                                      seconds=startDelivery.serviceTime))
        return nodeList
    def sendResultToDB(self, finessResult):
        multiDeliveriesList = finessResult.multiDeliveriesList
        driverVehicleCount = 0
        for deliveryList in multiDeliveriesList:
            if len(deliveryList) ==2:
                continue
            for i in range(len(deliveryList)-1):
                starDelivery = deliveryList[i]
                endDelivery = deliveryList[i+1]
                nodeList = self.doAStarSearch(starDelivery,endDelivery)
                deliveryList[i+1].aStarNodeList = nodeList
                print(endDelivery)

            for i in range(len(deliveryList)):
                delivery = deliveryList[i]
                jsonDelivery = {}
                jsonDelivery["orderId"] = delivery.order.orderId
                jsonDelivery["deliveryTime"] = delivery.deliveryTime
                jsonDelivery["serviceTime"] = delivery.serviceTime
                jsonDelivery["routes"] = []
                jsonDelivery["driverId"] = delivery.driverVehicle.driverId
                jsonDelivery["vehicleId"] = delivery.driverVehicle.vehicleId
                travellingTime = 0

                if i!=0:
                    previousDelivery = deliveryList[i-1]
                    travellingTime = diffBetweenDateTime(delivery.deliveryTime,previousDelivery.deliveryTime) - previousDelivery.serviceTime
                jsonDelivery["travellingTime"] =travellingTime
                if delivery.aStarNodeList != None:
                    for aStarNodeIndex in range(len(delivery.aStarNodeList)-1):
                        startIndex = delivery.aStarNodeList[aStarNodeIndex].node.nodeId
                        endIndex = delivery.aStarNodeList[aStarNodeIndex+1].node.nodeId
                        edge = edgeDataset.getEdgeByStartPointAndEndPoint(startIndex,endIndex)
                        jsonDelivery["routes"].append(edge.v_id)

                jsonData = json.dumps(jsonDelivery,default = myconverter)
                # print("-----------")
                r = requests.post(Common.apiPath +"deliveries/storeDeliveries", json=jsonData)


    def sendDynamicResultToDB(self, currentDynamicDriverList):

        # dynamicDriverList = dynamicFinessResult.dynamicDriverList

        for dynamicDriver in currentDynamicDriverList:
            if len(dynamicDriver.remainDeliveries) == 2:
                continue
            remainDeliveries = dynamicDriver.remainDeliveries
            for i in range(len(remainDeliveries)-1):
                starDelivery = remainDeliveries[i]
                endDelivery = remainDeliveries[i+1]
                nodeList = self.doAStarSearch(starDelivery,endDelivery)
                remainDeliveries[i+1].aStarNodeList = nodeList

            for i in range(len(remainDeliveries)):
                delivery = remainDeliveries[i]
                jsonDelivery = {}
                jsonDelivery["orderId"] = delivery.order.orderId
                if delivery.order.orderId == -1:
                    jsonDelivery['startLat'] = dynamicDriver.vehicleLocation.lat
                    jsonDelivery['startLng'] = dynamicDriver.vehicleLocation.lng

                jsonDelivery["deliveryTime"] = delivery.deliveryTime
                jsonDelivery["serviceTime"] = delivery.serviceTime
                jsonDelivery["routes"] = []
                jsonDelivery["driverId"] = dynamicDriver.driverVehicle.driverId
                jsonDelivery["vehicleId"] = dynamicDriver.driverVehicle.vehicleId
                travellingTime = 0

                if i != 0:
                    previousDelivery = remainDeliveries[i - 1]
                    travellingTime = diffBetweenDateTime(delivery.deliveryTime,
                                                         previousDelivery.deliveryTime) - previousDelivery.serviceTime
                jsonDelivery["travellingTime"] = travellingTime

                if delivery.aStarNodeList != None:
                    for aStarNodeIndex in range(len(delivery.aStarNodeList) - 1):
                        startIndex = delivery.aStarNodeList[aStarNodeIndex].node.nodeId
                        endIndex = delivery.aStarNodeList[aStarNodeIndex + 1].node.nodeId
                        edge = edgeDataset.getEdgeByStartPointAndEndPoint(startIndex, endIndex)
                        jsonDelivery["routes"].append(edge.v_id)

                jsonData = json.dumps(jsonDelivery, default=myconverter)
                print("-----------")
                # print(jsonData)
                r = requests.post(Common.apiPath + "deliveries/updateDynamicDeliveries", json=jsonData)
                print(r.text)

        

resultService = ResultService()
