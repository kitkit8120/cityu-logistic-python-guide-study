import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir)
import collections
from utils.SingletonMetaclass import SingletonMetaclass

class DynamicEnvironmentService(metaclass=SingletonMetaclass):
    def __init__(self):
        pass


    def constaint1(self, tempDynamicDriverList):
        for dynamicDriver in tempDynamicDriverList:
            carCapacity = dynamicDriver.driverVehicle.capacity
            deliveryList = dynamicDriver.remainDeliveries
            totalCapcity = 0.0
            for delivery in deliveryList:
                totalCapcity += delivery.order.capacity
            if carCapacity < totalCapcity:
                return False

        return True

    # num of node
    def constaint2(self,tempDynamicDriverList, orginalDynamicDriverList):
        for i in range(len(tempDynamicDriverList)):
            tempDynamicDriver = tempDynamicDriverList[i]
            orginalDynamicDriver = orginalDynamicDriverList[i]

            tempOrderList = []
            orginalOrder = []
            for delivery in tempDynamicDriver.remainDeliveries:
                tempOrderList.append(delivery.order)
            for delivery in orginalDynamicDriver.remainDeliveries:
                orginalOrder.append(delivery.order)
            # print('tempOrderList',tempOrderList)
            # print('orginalOrder', orginalOrder)

            for order in orginalOrder:
                temp = next((o for o in tempOrderList if o.orderId == order.orderId), None)

            if temp == None:
                return False

            duplications = [item for item, count in collections.Counter(tempOrderList).items() if count > 1]

            if len(duplications)>0:
                if duplications[0].orderId !=0:
                    return False
        return True


    def constaint4(self,tempDynamicDriverList, endTimeWindow):
        for tempDynamicDriver in tempDynamicDriverList:
            lastDelivery = tempDynamicDriver.remainDeliveries[-1]
            if lastDelivery.deliveryTime > endTimeWindow:
                return False
        return True


    def isOrderStructureValid(self,tempDynamicDriverList, orginalDynamicDriverList):
        
        return self.constaint1(tempDynamicDriverList) and self.constaint2(tempDynamicDriverList,orginalDynamicDriverList)
    
    def isTimeWindowsVaild(self,tempDynamicDriverList,endTimeWindow):
        return self.constaint4(tempDynamicDriverList,endTimeWindow)

dynamicEnvironmentService = DynamicEnvironmentService()
