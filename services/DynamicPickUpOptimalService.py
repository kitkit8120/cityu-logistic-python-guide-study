import multiprocessing
import os, sys, inspect
from multiprocessing.pool import ThreadPool

from excpetions.TimeWindowExceeded import TimeWindowExceeded
from modelObjects.Delivery import Delivery
from modelObjects.DynamicFitnessObject import DynamicFitnessObject
from services.TravellingService import travellingService

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0, parentdir)


from Common import Common
from utils.SingletonMetaclass import SingletonMetaclass

import copy

import time
from dataset.DynamicDriverDataset import dynamicDriverDataset
from dataset.OrderDataset import orderDataset
from services.DynamicEnvironmentService import dynamicEnvironmentService


class DynamicPickUpOptimalService(metaclass=SingletonMetaclass):
    def __init__(self):
        self.thread = Common.thread

    def checkPopulation(self, mutliPopluation):
        populationDynamicDriver, dynamicDriver = mutliPopluation[0],mutliPopluation[1]
        # print("currentDynamicDriver",dynamicDriver)
        vaild, singleDynamicDriver = self.checkValidFromTempPath(populationDynamicDriver, dynamicDriver)
        print('Done one singleDynamicDriver')
        # print(singleDynamicDriver)
        if vaild:
            fitnessObject = DynamicFitnessObject(singleDynamicDriver, dynamicDriver)
            fitnessObject.routeFitness()
            return fitnessObject
        else:
            return None
    def capacityConstraint(self,population,carCapacity):

        deliveryList = population
        totalCapcity = 0.0
        for delivery in deliveryList:
            totalCapcity += delivery.order.capacity
        if carCapacity < totalCapcity:
            return False
        return True


    def checkValidFromTempPath(self, populationDynamicDriver, dynamicDriver):
        carCapacity = dynamicDriver.driverVehicle.capacity
        deliveryList = populationDynamicDriver.remainDeliveries
        vaild = self.capacityConstraint(deliveryList,carCapacity)

        if vaild == False:
            return False,None
        try:
            singleDeliveryList = travellingService.calucateTravelNodeFromDeliveryList(
                deliveryList, Common.startTimeWindow, Common.endTimeWindow)
        except TimeWindowExceeded:
            return False, None

        # print(singleDeliveryList)
        populationDynamicDriver.remainDeliveries = singleDeliveryList
        return True, populationDynamicDriver



    def insertDynamicOrderToCurrentDriverList(self, order, currentDynamicDriverList):
        populations = []
        deliveryObject = Delivery()

        deliveryObject.order = order
        deliveryObject.deliveryDate = None
        deliveryObject.deliveryTime = None
        deliveryObject.aStarNodeList = None
        deliveryObject.serviceTime = float(600)

        for dynamicDriver in currentDynamicDriverList:
            tempDeliveryObject = copy.deepcopy(deliveryObject)
            tempDeliveryObject.driverVehicle = dynamicDriver
            population = []
            for index in range(1,len(dynamicDriver.remainDeliveries)-1):
                tempDynamicDriver = copy.deepcopy(dynamicDriver)
                tempDynamicDriver.remainDeliveries.insert(index,tempDeliveryObject)
                population.append(tempDynamicDriver)
            populations.append(population)

        return populations



    def calucateFitnessValue(self, fitnessObject):

        fitnessObject.routeFitness()
        return fitnessObject

    def rankRoutes(self, currentGen):

        fitnessResults = []

        for population in currentGen:
            fitnessResults.append(self.calucateFitnessValue(population))

        fitnessResults.sort(key=lambda x: x.fitnessValue, reverse=True)

        return fitnessResults



    def run(self):
        start_time = time.time()
        print("Starting Running Dynamic Pickup optimization")

        currentDynamicDriverList = copy.deepcopy(dynamicDriverDataset.dynamicDriverList)
        for nonPlannedOorder in orderDataset.nonPlanedOrders:
            print('running for %s'%(nonPlannedOorder))
            driversPopulations = self.insertDynamicOrderToCurrentDriverList(nonPlannedOorder,currentDynamicDriverList)

            mutliPopulation = []
            for index in range(len(driversPopulations)):
                currentDynamicDriver = currentDynamicDriverList[index]
                valid, currentDynamicDriver = self.checkValidFromTempPath(currentDynamicDriver, currentDynamicDriver)

                dynamicDriverPopulations = driversPopulations[index]
                for population in dynamicDriverPopulations:
                    if currentDynamicDriver !=None:
                        mutliPopulation.append([population,currentDynamicDriver])

            print('Done computing original route\n')
            with multiprocessing.Pool((int(len(mutliPopulation)/2))) as p:
                results = p.map(self.checkPopulation, mutliPopulation)
            results = [i for i in results if i]
            newlist = sorted(results, key=lambda x: x.fitnessValue, reverse=True)
            if len(results) == 0:
                print("No any solutions \n")
            tempCurrentDynamicDriverList = []
            for dynamicDriver in currentDynamicDriverList:
                if dynamicDriver.driverVehicle.driverId == newlist[0].singleDynamicDriver.driverVehicle.driverId:
                    tempCurrentDynamicDriverList.append(newlist[0].singleDynamicDriver)
                else:
                    tempCurrentDynamicDriverList.append(dynamicDriver)
            currentDynamicDriverList = tempCurrentDynamicDriverList


        # print(currentDynamicDriverList)

        print('\n\nFinished dynamicFitnessObject')
        crt_time = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
        print("Using Time: %s\t%ds " % (crt_time, (time.time() - start_time)))
        # print("New total time of vehicle routes: %ss" % (tmpBestResult.totalDuration))
        return currentDynamicDriverList


dynamicPickUpOptimalService = DynamicPickUpOptimalService()
